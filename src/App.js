import React, { Component } from 'react';
import{ Provider } from 'react-redux';

import Gallery from './components/Gallery';
import SearchForm from './components/Searchform';
import Pagenation from './components/Pagenation';

import store from './config/store';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div>
          <header>
            <h1>Flicker Gallery</h1>
          </header>
          <SearchForm />
          <Pagenation />
          <Gallery />
        </div>
      </Provider>
    );
  }
}

export default App;
