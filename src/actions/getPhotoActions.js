import { FETCH_GALLERY, NEXT_PAGE, SEARCH_PHOTO } from './types';
import store from '../config/store';
import axios from 'axios';

import {
  URL_API_BASE,
  URL_SEARCH,
  URL_DEFAULT_SEARCH,
  URL_API_KEY,
  URL_FORMAT,
  URL_NO_JSON_CALLBACK,
  URL_EXTRAS,
  URL_PER_PAGE,
} from '../config/constants';

const createURL = (page = 1, text = undefined) => {
  if (!text)
    return `${URL_API_BASE}?
    method=${URL_DEFAULT_SEARCH}&
    api_key=${URL_API_KEY}&
    format=${URL_FORMAT}&
    nojsoncallback=${URL_NO_JSON_CALLBACK}&
    extras=${URL_EXTRAS}&
    per_page${URL_PER_PAGE}&
    page=${page}`

  return `${URL_API_BASE}?
    method=${URL_SEARCH}&
    api_key=${URL_API_KEY}&
    text=${text}&
    format=${URL_FORMAT}&
    nojsoncallback=${URL_NO_JSON_CALLBACK}&
    extras=${URL_EXTRAS}&
    per_page${URL_PER_PAGE}&
    page=${page}`
}

export const fetchGallery = () => dispatch => {
  axios.get(createURL(store.getState().photos.page, store.getState().photos.keyword))
    .then(res => dispatch({
      type: FETCH_GALLERY,
      galleryPhotos: res.data.photos.photo,
      page: res.data.photos.pages,
      pages: res.data.photos.pages
    }));
}

export const getNextPage = (page) => dispatch => {
  axios.get(createURL(page, store.getState().photos.keyword))
    .then(res => dispatch({
      type: NEXT_PAGE,
      galleryPhotos: res.data.photos.photo,
      page: page,
      pages: res.data.photos.pages
    }));
}

export const searchPhotos = (searchKeyword) => dispatch => {
  let text = encodeURIComponent(searchKeyword.keyword.trim())
  axios.get(createURL(1, text))
    .then(res => dispatch({
      type: SEARCH_PHOTO,
      galleryPhotos: res.data.photos.photo,
      keyword: text,
      page: res.data.photos.page,
      pages: res.data.photos.pages
    }));
}
