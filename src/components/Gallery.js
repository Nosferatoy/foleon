import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchGallery } from '../actions/getPhotoActions';
import alt_image from '../assets/alt.png'
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

const style = {
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  photo: {
    padding: '5px',
    margin: '0 auto'
  }
};

class Gallery extends Component {

  componentWillMount() {
    this.props.fetchGallery();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.newPhotos) {
      Array.prototype.push.apply(this.props.photos, nextProps.newPhotos);
    }
  }

  render() {
    const flickrphotos = this.props.photos.map((photo, index) => (
      <div style={style.photo} key={index}>
        <a href={photo.url_l} rel="noopener noreferrer" target="_blank">
          <LazyLoadImage effect="blur" src={photo.url_m} alt={alt_image} width={200} height={200} />
        </a>
      </div>
    ));

    return (
      <div style={style.container}>
        {flickrphotos}
      </div>
    );

  }

}

const mapStateToProps = state => ({
  photos: state.photos.items,
  pages: state.photos.pages,
  page: state.photos.page
});

export default connect(mapStateToProps, { fetchGallery })(Gallery);
