import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getNextPage } from '../actions/getPhotoActions';
import Pagination from 'rc-pagination';
import 'rc-pagination/assets/index.css';

class Pagenation extends Component {

  constructor(props) {
    super(props);
  };

  onChange = (page) => {
    this.props.getNextPage(page);
  }

  render() {
    return (
      <div>
        {this.props.pages && <Pagination onChange={this.onChange} current={this.props.page} total={this.props.pages * 10} />}
      </div>);
  }
}

const mapStateToProps = state => ({
  page: state.photos.page,
  pages: state.photos.pages
});

export default connect(mapStateToProps, { getNextPage })(Pagenation);
