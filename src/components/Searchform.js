import React, { Component } from 'react';
import { connect } from 'react-redux';
import { searchPhotos } from '../actions/getPhotoActions';

class SearchForm extends Component {

  constructor(props){
    super(props);
    this.state = {
      keyword: '',
    }
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  };

  onChange(e){
    this.setState({[e.target.name]: e.target.value});
  }

  onSubmit(e){
    e.preventDefault();
    const keyword = {
      keyword: this.state.keyword
    }
    this.props.searchPhotos(keyword);
  }

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <input type="text" name="keyword" onChange={this.onChange} placeholder="Keyword" value={this.state.keyword} />
          <button type="submit">Search</button>
        </form>
      </div>
    );
  }
}

export default connect(null, {searchPhotos})(SearchForm);
