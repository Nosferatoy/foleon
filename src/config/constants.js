export const URL_API_BASE = 'https://api.flickr.com/services/rest/';
export const URL_SEARCH = 'flickr.photos.search';
export const URL_DEFAULT_SEARCH = 'flickr.photos.getRecent';
export const URL_API_KEY = '8c4528ce77599d8271470806aaaa97f0';
export const URL_FORMAT = 'json';
export const URL_NO_JSON_CALLBACK = '1';
export const URL_EXTRAS = 'url_m,url_l';
export const URL_PER_PAGE = '100';
