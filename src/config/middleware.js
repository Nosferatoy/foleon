import { applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

const getMiddlewareThunk = () => {
    return applyMiddleware(thunk);
};

const getMiddleware = () => {
    return getMiddlewareThunk();
};

export default getMiddleware;