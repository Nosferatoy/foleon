import { createStore } from 'redux';
import rootReducer from '../reducers';
import getMiddleware from './middleware';

const initialState = {};

const store = createStore(
  rootReducer,
  initialState,
  getMiddleware()
);

export default store;