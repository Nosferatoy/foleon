import { FETCH_GALLERY, NEXT_PAGE, SEARCH_PHOTO } from '../actions/types';

const initialState = {
  items: [],
  keyword: '',
  page: 1,
  pages: undefined
}

export default function(state = initialState, action) {
  console.log("action", action)
  switch (action.type) {
    case FETCH_GALLERY:
      return {
       ...state,
       items: action.galleryPhotos,
       page: action.page,
       pages: action.pages
      };
    case NEXT_PAGE:
      return {
       ...state,
       items: action.galleryPhotos,
       page: action.page,
       pages: action.pages
      };
    case SEARCH_PHOTO:
      return {
       ...state,
       items: action.galleryPhotos,
       keyword: action.keyword,
       page: action.page,
       pages: action.pages
      };
    default:
      return state;
  }
}